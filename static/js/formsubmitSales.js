// get form div containter
var container  = document.querySelector(".form-outer-container");
var formContainer = document.querySelector(".form-container");
var success = document.createElement("div");
var successMessage = "Product submited successfully";
var textNode = document.createTextNode(successMessage);
success.appendChild(textNode);
success.classList.add('alert', 'alert-success');
// get button element
var button = document.querySelector("#submit");

button.addEventListener("click", function(){
    // get data from form
    var form = document.querySelector("form");
    var formData = new FormData(form);
    // submit form data
    var request = new XMLHttpRequest();
    request.open('POST', '/sales', true);
    request.send(formData);
    request.onreadystatechange = function() {
        console.log(request.readyState);
        console.log(request.status);
        console.log(request.responseText);

        var formInput = {
            "date": document.querySelector('#date'),
            "department": document.querySelector('#department'),
            "product_name": document.querySelector('#product_name'),
            "product_code": document.querySelector('#product_code'),
            "quantity": document.querySelector('#quantity'),
            "purchase_price": document.querySelector('#purchase_price'),
            "sales_price": document.querySelector('#sales_price'),
            "supplier": document.querySelector('#supplier'),
            "color": document.querySelector('#color'),
            "description": document.querySelector('#description')
        }

        if (request.readyState === 4) {
            if (request.status === 200) {
                container.insertBefore(success, formContainer);
                for (var key in formInput) {
                    formInput[key].value = " ";
                }

            } else {
                alert('There was the following error adding your time -- ' +
                      request.status + ' -- ' + request.responseText);
            }
        }
    }


}, false);
