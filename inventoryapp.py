#!/usr/bin/env python

from bottle import Bottle, run, jinja2_view as view, static_file, request
from pymongo import MongoClient

conn = MongoClient()
db = conn.kdream
inventory = db.inventory

app = Bottle()


@app.route('/static/<filepath:path>')
def server_static(filepath):
    """defines the location for static files(images, css, js, etc.)"""
    return static_file(filepath, root="./static/")


@app.get('/manage')
@view('manage')
def DataEntry():
    """defines main page for entering data into the inventory database"""
    TITLE = "Add to inventory"
    return dict(TITLE=TITLE)


@app.post('/manage')
def submitDataEntry():
    """Recieves request from DataEntry page and populates the database"""
    DATA = {
        "date": request.forms.get('date'),
        "department": request.forms.get('department'),
        "product_name": request.forms.get('product_name'),
        "product_code": request.forms.get('product_code'),
        "quantity": int(request.forms.get('quantity')),
        "purchase_price": float(request.forms.get('purchase_price')),
        "sales_price": float(request.forms.get('sales_price')),
        "supplier": request.forms.get('supplier'),
        "color": request.forms.get('color'),
        "description": request.forms.get('description')
    }
    inventory.insert(DATA)
    return


@app.get('/sales')
@view('salesentry')
def SalesEntry():
    """defines page for updating inventory by salesperson"""
    TITLE = "Update inventory"
    return dict(TITLE=TITLE)


@app.post('/sales')
@view('salesentry')
def submitSalesEntry():
    """Recieves request from SalesEntry page and updates database"""
    TITLE = "Update inventory"
    DATA = {
        "salesperson": request.forms.get('salesperson'),
        "date": request.forms.get('date'),
        "product_code": request.forms.get('product_code'),
        "quantity": int(request.forms.get('quantity'))
    }
    inventory.update({"product_code": DATA["product_code"]},
                              {"$inc": {"quantity": -DATA["quantity"]}})
    RESULT = inventory.find_one({"product_code": DATA["product_code"]})
    return dict(RESULT=RESULT,TITLE=TITLE)


@app.get('/inventory')
@view('inventory')
def listProducts():
    TITLE = "Iventory"
    RESULT = inventory.find({ }, {"_id": 0})
    return dict(RESULT=RESULT,TITLE=TITLE)


run(app, host='localhost', port=8111, debug=True)
