{% include 'header.tpl' %}
{% include 'nav.tpl' %}
    <div class="form-outer-container">
      <div class="form-container">

        <form action="sales" method="post" class="form-horizontal" role="form">

            <div class="form-group">
                <label for="salesperson" class="col-sm-3 control-label">Salesperson</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="salesperson" name="salesperson" placeholder="Salesperson">
                </div>
            </div>

            <div class="form-group">
                <label for="date" class="col-sm-3 control-label">Date</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" id="date" name="date" placeholder="Date">
                </div>
            </div>

            <div class="form-group">
                <label for="product_code" class="col-sm-3 control-label">product code</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="product_code" name="product_code" placeholder="Prodcut Code">
                </div>
            </div>

            <div class="form-group">
                <label for="quantity" class="col-sm-3 control-label">quantity</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" id="submit" class="btn btn-default pull-right">Update Inventory</button>
                </div>
            </div>

    </form>

    </div>

</div>

<script src="/static/js/formsubmitSales.js"></script>
  </body>
</html>
