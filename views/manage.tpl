{% include 'header.tpl' %}
{% include 'nav.tpl' %}
    <div class="form-outer-container">
      <div class="form-container">

        <form action="manage" method="post" class="form-horizontal" role="form">

            <div class="form-group">
                <label for="Date" class="col-sm-3 control-label">Date</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" id="date" name="date" placeholder="Date">
                </div>
            </div>

            <div class="form-group">
                <label for="department" class="col-sm-3 control-label">Department</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="department" name="department" placeholder="Department">
                </div>
            </div>

            <div class="form-group">
                <label for="product_name" class="col-sm-3 control-label">Product Name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name">
                </div>
            </div>

            <div class="form-group">
                <label for="product_code" class="col-sm-3 control-label">Product code</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="product_code" name="product_code" placeholder="Prodcut Code">
                </div>
            </div>

            <div class="form-group">
                <label for="quantity" class="col-sm-3 control-label">Quantity</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                </div>
            </div>

            <div class="form-group">
                <label for="purchase_price" class="col-sm-3 control-label">Purchase price</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="purchase_price" name="purchase_price" placeholder="Purchase Price">
                </div>
            </div>

            <div class="form-group">
                <label for="sales_price" class="col-sm-3 control-label">Sales price</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="sales_price" name="sales_price" placeholder="Sales Price">
                </div>
            </div>

            <div class="form-group">
                <label for="supplier" class="col-sm-3 control-label">Supplier</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="supplier" name="supplier" placeholder="Supplier">
                </div>
            </div>

            <div class="form-group">
                <label for="color" class="col-sm-3 control-label">Color</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="color" name="color" placeholder="Color">
                </div>
            </div>


            <div class="form-group">
                <label for="description" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="3" placeholder="Description" name="description" id="description"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" id="submit" class="btn btn-default pull-right">Add Item</button>
                </div>
            </div>

    </form>

    </div>

</div>

<footer>
    <div class="footersocial"></div>
</footer>
<script src="/static/js/formsubmitManage.js"></script>
  </body>
</html>
