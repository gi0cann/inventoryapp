<!doctype html>
<head>
    <title>Inventory</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
{% include 'nav.tpl' %}
<div>
<table class="table table-hover table-bordered">
    <tr>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Purchase Price</th>
        <th>Sales Price</th>
        <th>Color</th>
        <th>Quantity</th>
        <th>Supplier</th>
        <th>Date</th>
        <th>Description</th>
    </tr>

{% for j in RESULT: %}
{%    set product_code = j["product_code"] %}
{%    set product_name = j["product_name"] %}
{%    set purchase_price = j["purchase_price"] %}
{%    set sales_price = j["sales_price"] %}
{%    set color = j["color"] %}
{%    set quantity = j["quantity"] %}
{%    set supplier = j["supplier"] %}
{%    set date = j["date"] %}
{%    set description = j["description"] %}
    <tr>
        <td>{{product_code}}</td>
        <td>{{product_name}}</td>
        <td>{{purchase_price}}</td>
        <td>{{sales_price}}</td>
        <td>{{color}}</td>
        <td>{{quantity}}</td>
        <td>{{supplier}}</td>
        <td>{{date}}</td>
        <td>{{description}}</td>
    </tr>
{% endfor %}
</table>
</div>

</body>
</html>
