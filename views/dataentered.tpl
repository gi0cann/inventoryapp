<!doctype html>
<head>
    <title>Data Entered</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
{% include 'nav.tpl' %}
<div>
<table class="table table-hover table-bordered">
    <tr>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Purchase Price</th>
        <th>Sales Price</th>
        <th>Color</th>
        <th>Quantity</th>
        <th>Supplier</th>
        <th>Date</th>
        <th>Description</th>
    </tr>

{% product_code = DATA["product_code"] %}
{% product_name = DATA["product_name"] %}
{% purchase_price = DATA["purchase_price"] %}
{% sales_price = DATA["sales_price"] %}
{% color = DATA["color"] %}
{% quantity = DATA["quantity"] %}
{% supplier = DATA["supplier"] %}
{% date = DATA["date"] %}
{% description = DATA["description"] %}
    <tr>
        <td>{{product_code}}</td>
        <td>{{product_name}}</td>
        <td>{{purchase_price}}</td>
        <td>{{sales_price}}</td>
        <td>{{color}}</td>
        <td>{{quantity}}</td>
        <td>{{supplier}}</td>
        <td>{{date}}</td>
        <td>{{description}}</td>
    </tr>
</table>
</body>
</html>
