<!doctype html>
<head>
    <title>Saled Updated</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
{% include 'nav.tpl' %}
<div>
<table class="table table-hover table-bordered">
    <tr>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Purchase Price</th>
        <th>Sales Price</th>
        <th>Color</th>
        <th>Quantity</th>
        <th>Supplier</th>
        <th>Date</th>
        <th>Description</th>
    </tr>

{% set product_code = RESULT["product_code"] %}
{% set product_name = RESULT["product_name"] %}
{% set purchase_price = RESULT["purchase_price"] %}
{% set sales_price = RESULT["sales_price"] %}
{% set color = RESULT["color"] %}
{% set quantity = RESULT["quantity"] %}
{% set supplier = RESULT["supplier"] %}
{% set date = RESULT["date"] %}
{% set description = RESULT["description"] %}
    <tr>
        <td>{{product_code}}</td>
        <td>{{product_name}}</td>
        <td>{{purchase_price}}</td>
        <td>{{sales_price}}</td>
        <td>{{color}}</td>
        <td>{{quantity}}</td>
        <td>{{supplier}}</td>
        <td>{{date}}</td>
        <td>{{description}}</td>
    </tr>
</table>
</body>
</html>
